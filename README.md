# README #

Opinia Census socket server for real time comments and chat
1. Only clients can create chat with there followers (Group or one to one)
2. Client and/or a user can comment around any activity

### What is this repository for? ###

Socket server coded with NodeJS, Express, MySQL and Socket.IO
version 1.0

### How do I get set up? ###

Pull the branch, run npm install to install all the dependencies
Run command npm start
Common settings can be found in main.js

### WARNING ###
DO NOT USE THIS CODE IN PRODUCTION. WE HAVE REMOVED A LOTS OF CODE/LOGIC RELATED TO DATABASE AND SECURITY. IF YOU STILL WANT TO USE THIS CODE, PLEASE GO THROUGH PROPER TESTING