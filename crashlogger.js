#!/usr/bin/env node
/* Opiniacensus chat and comments socket server
 * Coded by: Tejashwi Kalp Taru
 * contact: tejashwi@opinia360.com
 */

const nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var fs = require('fs');
var transporter = null;

/* varibale setup */
var mailError = false;
var mailUser = "";
var mailPassword = "";
var mailHost = "";
var mailPort = 465;
var mailSecure = true; // secure:true for port 465, secure:false for port 587
var rejectUnauthorized = false;

// create reusable transporter object using the default SMTP transport
if(mailError){
    transporter = nodemailer.createTransport(smtpTransport, {
        host: mailHost,
        port: mailPort,
        secure: mailSecure,
        auth: {
            user: mailUser,
            pass: mailPassword
        },
        tls: {
            // do not fail on invalid certs
            rejectUnauthorized: rejectUnauthorized
        }
    });
}

// verify connection configuration
if(mailError){
    transporter.verify(function(error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log('Server is ready to mail crash logs');
        }
    });
}else{
    console.log('Server is ready to save crash logs')
}

var logError = function(report, fatal){
    // setup email data with unicode symbols
    if(mailError){
        var mailOptions = {
            from: "'Crash report 👻' <" + mailUser + ">", // sender address
            to: 'tejashwi@opinia360.com', // list of receivers
            subject: 'Opinia census socket log report ✔', // Subject line
            html: '<b>' + report + '</b>' // html body
        };
    
        //send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) return console.log(error);
            if(fatal) process.exit(1);
        });
    }else{
        fs.appendFile('error_log.txt', report, function (err) {
            if (err) throw err;
            if(fatal) process.exit(1);
        });
    }
}

exports.init = function(){
    process.on('uncaughtException', function(err){
        // handle the error safely
        if(mailError) err = err + '<br><br>Stack Trace: <br>' + err.stack;
        else err = "\n\n" + err + "\nStack Trace: \n" + err.stack; 
        logError(err, true);
    });

    //show warning trace also
    process.on('warning', e => console.warn(e.stack));
}

exports.saveLog = function(err, from, skip_trace){
    var e = new Error();
    from += " Time: " + Date().toString();
    if (typeof(skip_trace) === 'undefined'){
        if(mailError) err = err+ "<br>Stack Trace:<br>" + e.stack;
        else err = err+ "\nStack Trace:\n" + e.stack;
    }
    if(mailError) logError("<br><b>ERROR GENERATED FROM: " + from + "</b><br><br>" + err);
    else logError("\n\nERROR GENERATED FROM: " + from + "\n\n" + err, false);
}