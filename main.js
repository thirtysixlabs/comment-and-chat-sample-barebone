#!/usr/bin/env node
/* Opiniacensus chat and comments socket server
 * Coded by: Tejashwi Kalp Taru
 * contact: tejashwi@opinia360.com
 *          tejashwi@thirtysixlabs.com
 */

/* varibale setup !!! REQUIRED !!! */
var db_connect = "local"; //local, staging, production
var port = 5000;
var ngrok = require('ngrok');

/* Server setup */
require('events').EventEmitter.prototype._maxListeners = 1000000;
//process.setMaxListeners(Infinity);
var app = require('express')();
var fs = require('fs');
var https  = require('https');
var http = require('http');
var server = null;
var io = null;

var startServer = function(server, port, use_ssl, db_connect, app, show_err_publically, public_url){
    server.listen(port, function(){
        if(!use_ssl) console.log('Census socket started on port ' + port + ", not using SSL");
        else console.log('Census socket started on port ' + port + ", using SSL");
    });

    var crashlogger = require('./crashlogger.js');
    crashlogger.init();

    var database = require('./database.js');
    database.initDatabase(db_connect, crashlogger);

    var routes = require('./routes.js');
    routes.setupRoutes(app, database, crashlogger, show_err_publically, public_url);

    //https://github.com/socketio/socket.io/issues/2945#issuecomment-302213599
    //degraded socket.io version to 1.7.4
    io = require('socket.io').listen(server);//, { wsEngine: 'ws' });
    var sockets = require('./socket.js')(io, database, crashlogger);
}

if(db_connect == "local"){
    //we are running on local server for testing purpose,
    //start ngrok and get public url for testing
    ngrok.connect({
        proto: 'http',
        addr: port,
        authtoken: '',
        subdomain: '',
        region: ''
    }, function (err, url) {
        if(err){
            console.log("Unable to start ngrok");
            console.log(err);
        }else{
            var public_url = url;
            console.log("Public URL generated: " + url);
            //we are running locally, don't use SSL
            server = http.createServer(app);
            if(server && app)
                startServer(server, port, false, db_connect, app, true, public_url);
            else console.log("Unable to start server! Check server and app");
        }
    });
}else{
    //we are not running locally, must be on server. Use SSL
    server = https.createServer({
        key: fs.readFileSync(__dirname + '/certs/private.key'),
        cert: fs.readFileSync(__dirname + '/certs/certificate.crt'),
        ca: fs.readFileSync(__dirname + '/certs/ca_bundle.crt'),
        requestCert: false,
        rejectUnauthorized: false
    },app);
    if(server && app)
        startServer(server, port, true, db_connect, app, false, "https://opiniacensus.com:5000");
    else console.log("Unable to start server! Check server and app");
}