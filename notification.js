#!/usr/bin/env node
/* Opiniacensus chat and comments socket server
 * Coded by: Tejashwi Kalp Taru
 * contact: tejashwi@opinia360.com
 */

var FCM = require('fcm-node');
var apn = require('apn');
var webpush = require('web-push');
var error_logger = null;

var options = {
    token: {
        key: "./certs/ios_prod.p8",
        keyId: "",
        teamId: ""
    },
    production: true
};
var apnProvider = new apn.Provider(options);

var logError = function(err, log){
    console.log(err);
    if(error_logger && log) error_logger.saveLog(err, "notification.js");
}


var sendFCM = function(push_token, title, obj){
    var serverKey = '';
    var fcm = new FCM(serverKey);
 
    var message = {
        to: push_token,
        notification: {
            title: 'Opinia Census', 
            body: title 
        },
        data: obj
    };
    
    fcm.send(message, function(err, response){
        if (err) {
            logError("Unable to send FCM: " + err, true);
        } else {
            logError("FCM notification sent: " +  response, false);
        }
    });
}

var sendApple = function(push_token, title, obj){
    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now
    note.sound = "ping.aiff";
    note.topic = "";
    note.alert = title;

    note.payload = obj;

    apnProvider.send(note, push_token).then( (result) => {
        // see documentation for an explanation of result
        logError("iOS notification sent: " + JSON.stringify(result), true);
    });
}

var sendWeb = function(endurl, key, auth, msg){
    var legacyServerKey = "";
    var serverKey = "";

    var publicKey = "";
    var privateKey = "";

    webpush.setGCMAPIKey(serverKey);
    webpush.setVapidDetails('mailto:info@thirtysixlabs.com', publicKey, privateKey);
    
    // This is the same output of calling JSON.stringify on a PushSubscription
    var pushSubscription = {
        endpoint: endurl,
        keys: {
            auth: auth,
            p256dh: key
        }
    };
    
    webpush.sendNotification(pushSubscription, msg).then(function(response){
        logError(response, false);
    }).catch(function(error){
        logError(error, true);
    });
}

exports.logger = function(logger){
    error_logger = logger;
    if(error_logger) error_logger.init();
}

exports.sendClientNotification = function(database, uid, payload){
    database.getPushtokenClient(uid, function(err, ret){
        if(err){
            logError(err, true);
        }else{
            if(ret){
                if(ret.endpoint && ret.auth && ret.p256dh){
                    sendWeb(ret.endurl, ret.p256dh, ret.auth, payload);
                }
            }
        }
    })
}

exports.sendNotification = function(database, send_to, sender, title, msg){
    if(send_to != 0 && sender != 0 && send_to != sender){
        database.getPushtokenUser(send_to, function(err, ret){
            if(err){
                logError('Push token not fetched: ' + err, true);
            }else{
                if(ret){
                    //TODO: INSERT NOTIF INTO DB
                    var device_type = ret.device_type;
                    var push_token = ret.push_token;
                    if(device_type == 1){
                        //send to android
                        sendFCM(push_token, title, msg);
                    }
                    if(device_type == 2){
                        //send to apple
                        var re = /[0-9A-Fa-f]{64}/g;
                        if(re.test(push_token)){
                            //valid apple token, 64 hex chars 
                            sendApple(push_token, title, msg);
                        } else {
                            logError('invalid push token: ' + push_token + " for uid: " + send_to, true);
                        }
                        re.lastIndex = 0;
                    }
                }
            }
        });
    }
}