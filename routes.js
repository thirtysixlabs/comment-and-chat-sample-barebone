#!/usr/bin/env node
/* Opiniacensus chat and comments socket server
 * Coded by: Tejashwi Kalp Taru
 * contact: tejashwi@opinia360.com
 */

var show_error_publically = false;
var bodyParser = require('body-parser');
var cors = require('cors');
var fs = require('fs');
var path = require('path');
var formidable = require('formidable');
var intformat = require('biguint-format');
var FlakeId = require('flake-idgen')
var error_logger = null;
var flakeIdGen = null;
var qt = require('quickthumb'); //ImageMagick should be installed on the server
var pub_url = '';
var notif = require('./notification.js');

var logError = function(res, err, log){
    if(res){
        if(show_error_publically) res.set('Content-Type', 'application/json').status(503).send(err);
        else res.set('Content-Type', 'application/json').status(503).send("The server is currently unavailable, we have notified engineering team for further investigation, this may be due to some internal error or any invalid activity from your side");
    }
    if(error_logger && log) error_logger.saveLog(err, "routes.js");
}

//safe side request params validator
var validate = function(val){
    return typeof(val) === 'undefined' ? 0 : val;
}

//check if given file name is an image
var imageFile = function(file){
    var ext = file.split('.').pop();
    ext = ext.toLowerCase().trim();
    //do not try to compress or thumbnail GIF images
    return ext.match(/(jpg|jpeg|png|bmp)$/i)
}

//check if file extension is allowed to upload
var isAllowedType = function(ext){
    ext = ext.toLowerCase().trim();
    return ext.match(/(pdf|docx|doc|xls|xlsx|csv|ppt|pptx|jpg|jpeg|gif|png|bmp|txt|rtf|mp3|aac)$/i)
}

//helper function to create group
var createGroup = function(database, group_name, uid, is_client, group_type, group_users, res){
    if(group_name == 0) group_name = "unnamed-group";
    database.createGroup(uid, is_client, group_name, group_type, function(err, ret){
        if(err) logError(res, err, true);
        else{
            if(ret){
                var group_id = ret;
                group_users.forEach(element => {
                    database.addParticipant(group_id, element, 0, null);
                    //send notficication that user is added
                    var notif_msg = {
                        'msg' : 'Added in a chat',
                        'hash' : ''
                    };
                    notif.sendNotification(database, element, "0xDEADBEEF", "You are added in a chat", notif_msg);
                });
                //add client, send reply only we assure that one user is added
                database.addParticipant(group_id, uid, 1, function(err, ret){
                    if(err) logError(res, err, true);
                    else{
                        database.getSingleRoom(uid, is_client, group_id, function(err, ret){
                            if(err){
                                logError(res, err, true);
                            }else{
                                var reply = {roomid:group_id, data: ret};
                                reply = JSON.stringify(reply);
                                res.set('Content-Type', 'application/json').status(200).send(reply);
                            }
                        });
                    }
                });
            }else{
                res.set('Content-Type', 'application/json').status(401).send('Unable to create room');
            }
        }
    });
}

exports.setupRoutes = function(app, database, crash_logger, show_error_public, public_url){
    show_error_publically = show_error_public;
    error_logger = crash_logger;
    if(error_logger) error_logger.init();
    notif.logger(crash_logger);
    flakeIdGen = new FlakeId({ epoch: +new Date()/1000 });
    pub_url = public_url;
    
    app.use(bodyParser.json()); // support json encoded bodies
    app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


    /****************************** MISC *******************************/

    app.get('/ver/', function(req, res){
        res.set('Content-Type', 'text/html').status(200).send("OpiniaCensus socket server v1.0.0 - Tejashwi Kalp Taru");
    });

    app.get('/showlogs/', function(req, res){
        if(req.query.key == "fuckoff21"){
            var logs = fs.readFileSync(__dirname + '/error_log.txt');
            if(logs.length) res.status(404).sendFile(__dirname + '/error_log.txt');
            else res.set('Content-Type', 'text/html').status(200).send("No log, create some error!");
        }else{
            res.status(404).sendFile(__dirname + '/404.html');
        }
    });

    app.get('/clearlogs/', function(req, res){
        if(req.query.key == "fuckoff21"){
            fs.truncate(__dirname + '/error_log.txt', 0, function(){
                res.set('Content-Type', 'text/html').status(200).send("Error logs cleared");
                var currentdate = new Date(); 
                var datetime = "Log cleared at: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
                if(error_logger) error_logger.saveLog("Error log cleared", datetime, true);
            });
        }else{
            res.status(404).sendFile(__dirname + '/404.html');
        }
    })

    app.get('/', function(req, res){
        res.status(200).sendFile(__dirname + '/index.html');
    });

    app.post('/getfollowers/', cors(),function(req, res){
        var uid = validate(req.body.uid);
        var key = validate(req.body.api_key);
        var is_client = parseInt(validate(req.body.is_client));

        database.authenticateUser(uid, key, is_client, function(err, ret){
            if(err) logError(res, err, true);
            else{
                if(ret){
                    database.getFollowers(uid, function(err, ret){
                        res.set('Content-Type', 'application/json').status(200).send(JSON.stringify(ret));
                    });
                }
                else res.set('Content-Type', 'application/json').status(401).send({"message" : "Invalid user API Key"});
            }
        });
    });


    /****************************** COMMENTS *******************************/

    //get all the comments related to activity, allow CORS
    app.get('/comments/:id', cors(), function(req, res){
        database.getAllComments(req.params.id, function(err, ret){
            if(err) logError(res, err, true);
            else res.set('Content-Type', 'application/json').status(200).send(ret);
        });
    });

    //delete a comment or reply
    app.post('/deletecomment/', cors(),function(req, res){
        var uid = validate(req.body.uid);
        var key = validate(req.body.api_key);
        var cid = validate(req.body.cid);
        //only admin can delete comment
        var is_client = 1;//parseInt(validate(req.body.is_client));

        database.authenticateUser(uid, key, is_client, function(err, ret){
            if(err) logError(res, err, true);
            else{
                if(ret){
                    database.deleteComment(cid);
                    res.set('Content-Type', 'application/json').status(200).send({"message" : "deleted successfully"});
                }
                else res.set('Content-Type', 'application/json').status(401).send({"message" : "Invalid user API Key"});
            }
        });
    });


    /****************************** CHATS *******************************/

    //get messages for a chat group/room
    app.post('/getmessages/', cors(), function(req, res){
        var uid = validate(req.body.uid);
        var key = validate(req.body.api_key);
        var is_client = parseInt(validate(req.body.is_client));
        var group = validate(req.body.group_id);
        var start = parseInt(validate(req.body.start));
        var end = parseInt(validate(req.body.end));

        database.authenticateUser(uid, key, is_client, function(err, ret){
            if(err) logError(res, err, true);
            else{
                if(ret) {
                    database.getMessages(group, uid, is_client, start, end, function(err, ret){
                        if(err) logError(res, err, true);
                        else{
                            res.set('Content-Type', 'application/json').status(200).send(JSON.stringify(ret));
                        }
                    });
                }
                else res.set('Content-Type', 'application/json').status(401).send({"message" : "Invalid user API Key"});
            }
        });
    });
    
    //get all group/room
    app.post('/getrooms/', cors(), function(req, res){
        var uid = validate(req.body.uid);
        var key = validate(req.body.api_key);
        var is_client = parseInt(validate(req.body.is_client));

        database.authenticateUser(uid, key, is_client, function(err, ret){
            if(err) logError(res, err, true);
            else{
                if(ret) {
                    database.getRooms(uid, is_client, function(err, ret){
                        if(err) logError(res, err, true);
                        else res.set('Content-Type', 'application/json').status(200).send(JSON.stringify(ret));
                    });
                }
                else res.set('Content-Type', 'application/json').status(401).send({"message" : "Invalid user API Key"});
            }
        });
    });

    //create a new chat room
    app.post('/createroom/', cors(), function(req, res){
        var uid = validate(req.body.uid);
        var key = validate(req.body.api_key);
        //only admin can create groups
        var is_client = 1; //parseInt(validate(req.body.is_client));
        var group_name = validate(req.body.group_name);
        var group_type = validate(req.body.group_type);
        var group_users = validate(req.body.group_users).toString();
        group_users = group_users.split(",");

        database.authenticateUser(uid, key, is_client, function(err, ret){
            if(err) logError(res, err, true);
            else{
                if(ret){
                    if(typeof(group_users) === 'object' && group_type > 0){
                        if(group_type == 1){
                            //check if such one to one conversation already exists
                            database.singleChatExists(uid, group_users[0], function(err, ret){
                                if(err) logError(res, err, true);
                                else{
                                    if(ret){
                                        //chat already exists between these two users, reject request
                                        var reply = {roomid: ret, data: null, message: "Chat with this user already exists"};
                                        reply = JSON.stringify(reply);
                                        res.set('Content-Type', 'application/json').status(200).send(reply);
                                        return;
                                    }else{
                                        createGroup(database, group_name, uid, is_client, group_type, group_users, res);
                                    }
                                }
                            });
                        }else{
                            createGroup(database, group_name, uid, is_client, group_type, group_users, res);
                        }
                    }else{
                        res.set('Content-Type', 'application/json').status(401).send('Invalid group users and group type');
                    }
                }
                else res.set('Content-Type', 'application/json').status(401).send({"message" : "Invalid user API Key"});
            }
        });
    });

    //download, show original file @default->show 150x150 thumbnail if image or show file
    app.get('/downloads/:id', cors(), function(req, res){
        var file = req.params.id;
        var forceDownload = validate(req.query.download);
        var showOriginal = validate(req.query.show);

        var uploadDir = path.join(__dirname, '/uploads');
        var full_path =  path.join(uploadDir, file);

        fs.stat(full_path, function(err, stat) {
            if(err == null) {
                if(forceDownload){
                    //send the file to download
                    res.status(200).download(full_path);
                }
                else{
                    //if show original is requested or requested file is not an image, send it directly
                    if(showOriginal || !imageFile(full_path)) res.status(200).sendFile(full_path);
                    else{
                        //else send a thumbnail
                        var thumbDir = path.join(__dirname, '/uploads/thumbs/');
                        var thumb_path =  path.join(thumbDir, file);
                        //check if thumbnail exists
                        fs.stat(thumb_path, function(err, stat){
                            if(err == null){
                                res.status(200).sendFile(thumb_path);
                            }else if(err.code == 'ENOENT'){
                                //create thumb and send
                                var opts = {
                                    src: full_path,
                                    dst: thumb_path,
                                    width: 150,
                                    height: 150,
                                };
                                qt.convert(opts, function(e, p){
                                    if(e){
                                        logError(res, e, true);
                                    }else{
                                        res.status(200).sendFile(p);
                                    }
                                });
                            }else{
                                logError(res, err, true);
                            }
                        });
                    }
                }
            } else if(err.code == 'ENOENT') {
                res.set('Content-Type', 'application/json').status(404).send('Requested file not found on server');
            } else {
                logError(res, err, true);
            }
        });
    });

    //allow CORS preflight check
    app.options('/uploads/', function (req, res) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader('Access-Control-Allow-Methods', '*');
        res.setHeader("Access-Control-Allow-Headers", "*");
        res.end();
    });

    //upload file to server
    app.post('/uploads/', cors(), function(req, res){
        var uid = validate(req.query.uid);
        var key = validate(req.query.api_key);
        var is_client = parseInt(validate(req.query.is_client));
        var sizeLimitBytes = 15000000; //15MB in bytes

        database.authenticateUser(uid, key, is_client, function(err, ret){
            if(err) logError(res, err, true);
            else{
                if(ret){
                    var form = new formidable.IncomingForm();
                    form.multiples = false;
                    form.uploadDir = path.join(__dirname, '/uploads');

                    form.on('file', function(field, file) {
                        var ext = file.name.split('.').pop();
                        var new_name = "census-" + intformat(flakeIdGen.next(), 'dec').toString() + "." + ext;
                        var new_path =  path.join(form.uploadDir, new_name);
                        fs.rename(file.path, new_path, function(err){
                            if(err) logError(res, err, true);
                            else{
                                var type = new_path.split('.').pop();
                                if(isAllowedType(type)){
                                    var url = pub_url + "/downloads/" + new_name;
                                    res.set('Content-Type', 'application/json').status(200).send({status: "success", link: url});
                                }else{
                                    fs.unlink(new_path, function(err){
                                        if(err) logError(null, err, true);
                                    });
                                }
                            }
                        });
                    });

                    form.on('fileBegin', function(name, file){
                        var fname_ext = file.name.split('.').pop();
                        if(!isAllowedType(fname_ext)){
                            this.emit('error', 'Invalid file type, upload not supported. Type: ' + fname_ext + ' Tried by uid: ' + uid + ' isclient: ' + is_client);
                        }else{
                            if(form.bytesExpected > sizeLimitBytes){
                                this.emit('error', 'Size must not be over ' + sizeLimitBytes + ' bytes. Tried by uid:' + uid + ' client:' + is_client);
                            }
                        }
                    });

                    form.on('progress', function(bytesReceived, bytesExpected) {
                        if(bytesReceived > sizeLimitBytes ){
                            form.emit('error', new Error('Received higher bytes than allowed to upload. Tried by uid:' + uid + ' client:' + is_client));
                        }
                    });

                    // log any errors that occur
                    form.on('error', function(err) {
                        res.header('Connection', 'close');
                        logError(res, err, true);
                    });

                    // parse the incoming request containing the form data
                    form.parse(req);
                }
                else res.set('Content-Type', 'application/json').status(401).send({"message" : "Invalid user API Key"});
            }
        });
    });

    //list all the group users
    app.post('/groupusers/', cors(), function(req, res){
        var uid = validate(req.body.uid);
        var key = validate(req.body.api_key);
        var is_client = parseInt(validate(req.body.is_client));
        var group_id = validate(req.body.group_id);
        database.authenticateUser(uid, key, is_client, function(err, ret){
            if(err) logError(res, err, true);
            else{
                if(ret) {
                    database.findGroupUsers(group_id, function(err, ret){
                        if(err){
                            logError(err, res, true);
                        }else{
                            res.set('Content-Type', 'application/json').status(200).send({status: "success", users: ret});
                        }
                    });
                }
                else res.set('Content-Type', 'application/json').status(401).send({"message" : "Invalid user API Key"});
            }
        });
    });


    /****************************** Error Handling *******************************/
    //codes below must remain at the bottom, they handle 404 and 500 errors
    
    app.use(function(err, res){
        res.status(404).sendFile(__dirname + '/404.html');
    });

    app.use(function (err, req, res, next) {
        logError(res, err.stack, true);
    });
}