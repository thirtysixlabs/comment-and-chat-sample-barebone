#!/usr/bin/env node
/* Opiniacensus chat and comments socket server
 * Coded by: Tejashwi Kalp Taru
 * contact: tejashwi@opinia360.com
 */
//https://socket.io/docs/emit-cheatsheet/

var notif = require('./notification.js');
var md5 = require('md5');

var online_users = {};
var online_clients = {};
var client_group_list = {};
var user_group_list = {};
var error_logger = null;

var validate = function(val){
    return typeof(val) === 'undefined' ? 0 : val;
}

var s4 = function(){
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

var make_guid = function(){
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

//update online users list
var updateList = function(uid, socketid, chatwith, client){
    var guid = make_guid();
    var data = {'id':uid, 'socketid':socketid, 'chatwith':chatwith, 'auth': guid};
    if(client) online_clients[uid] = data;
    else online_users[uid] = data;
    return guid;
}

//return online user data
var getOnlineUser = function(uid, client){
    //check if user is added in online list
    if(client){
        if(uid in online_clients){
            return online_clients[uid];
        }else return false;
    }else{
        if(uid in online_users){
            return online_users[uid];
        }else return false;
    }
}

//check if user is authorized to post in a room
var authorizedRoom = function(socket, room_id){
    var rooms = [];
    if(socket.is_client){
        if(socket.uid in client_group_list){
            rooms = client_group_list[socket.uid];
        }
    }else{
        if(socket.uid in user_group_list){
            rooms = user_group_list[socket.uid];
        }
    }
    var i = rooms.indexOf(room_id.toString());
    if(i>-1) return true;
    else return false;
}

//deauth user from a room
var deauthorizeRoom = function(socket, room_id){
    var rooms = [];
    var rooms_rem = [];
    if(socket.is_client){
        if(socket.uid in client_group_list){
            rooms = client_group_list[socket.uid];
        }
    }else{
        if(socket.uid in user_group_list){
            rooms = user_group_list[socket.uid];
        }
    }
    for(var i=0;i<rooms.length;i++){
        if(rooms[i] == room_id){
            io.in('group-' + rooms[i]).emit('offline', {uid: socket.uid, client: socket.is_client, name: socket.name});
            socket.leave('group-' + rooms[i]);
        }else{
            rooms_rem.push(rooms[i]);
        }
    }
    if(socket.is_client){
        if(socket.uid in client_group_list){
            client_group_list[socket.uid] = rooms_rem;
        }
    }else{
        if(socket.uid in user_group_list){
            user_group_list[socket.uid] = rooms_rem;
        }
    }
}

//check if socket is authenticated via initChat
var authenticated = function(socket){
    //authentication
    var suid = socket.uid;
    var sauth = socket.auth;
    var client = socket.is_client;

    var suserdata = getOnlineUser(suid, client);
    if(suserdata === false){
        logError(suid + ' is not added to online list');
        return false;
    }else{
        if(suserdata.auth != sauth){
            logError(suid + ' auth key mismatch');
            return false;
        }
    }
    return true; //user is authenticated
}

var disconnectUser = function(io, socket){
    if(socket.is_client){
        delete online_clients[socket.uid];
        if(socket.uid in client_group_list){
            var rooms = client_group_list[socket.uid];
            for(var i=0;i<rooms.length;i++){
                io.in('group-' + rooms[i]).emit('offline', {uid: socket.uid, client: socket.is_client, name: socket.name});
                socket.leave('group-' + rooms[i]);
            }
            delete client_group_list[socket.uid];
        }
    }
    else{
        delete online_users[socket.uid];
        if(socket.uid in user_group_list){
            var rooms = user_group_list[socket.uid];
            for(var i=0;i<rooms.length;i++){
                io.in('group-' + rooms[i]).emit('offline', {uid: socket.uid, client: socket.is_client, name: socket.name});
                socket.leave('group-' + rooms[i]);
            }
            delete user_group_list[socket.uid];
        }
    }

    socket.room = 0;
    socket.auth = 0;
    socket.uid = 0;
    socket.type = 0;
    socket.is_client = 0;
    socket.name = '';
    socket.brand = null;
    socket.image = null;
}

var logError = function(err, log){
    console.log(err);
    if(error_logger && log) error_logger.saveLog(err, "socket.js");
}

var isOurURL = function(url){
    url = url.toLowerCase().trim();
    var patt_one = new RegExp("https://opiniacensus\\.com", "ig");
    var patt_two = new RegExp("https://[a-zA-Z0-9]*\\.ngrok\\.io", "ig");
    var res_one = patt_one.test(url);
    var res_two = patt_two.test(url);
    return (res_one || res_two);
}

var isAllowedFile = function(url){
    url = url.toLowerCase().trim();
    var ext = url.split('.').pop();
    return ext.match(/(pdf|docx|doc|ppt|pptx|csv|xls|xlsx|txt|rtf|mp3|aac)$/i)
}

var isAllowedImage = function(url){
    url = url.toLowerCase().trim();
    var ext = url.split('.').pop();
    return ext.match(/(jpg|jpeg|gif|png|bmp)$/i)
}


module.exports = function(io, database, crash_handler){
    error_logger = crash_handler;
    notif.logger(crash_handler);
    if(error_logger) error_logger.init();
    
    io.sockets.on('connection', function(socket){
        logError('anon user connected', false);
        socket.room = 0;
        socket.auth = 0;
        socket.uid = 0;
        socket.type = 0;
        socket.is_client = 0;
        socket.name = '';
        socket.brand = null;
        socket.image = null;

        socket.on('initComment', function(data){
            var uid = validate(data.uid);
            var key = validate(data.api_key);
            var is_client = parseInt(validate(data.is_client));
            var aid = validate(data.aid);

            database.authenticateUser(uid, key, is_client, function(err, ret){
                if(err){
                    logError(err, true);
                    socket.emit('on_error', {message: "An error occurred, we have notified engineering team"});
                }else{
                    if(ret){
                        if(is_client){
                            socket.name = ret.client_name;
                            socket.brand = ret.brand_name;
                            socket.image = ret.logo_image;
                        }else{
                            socket.name = ret.name;
                        }
                        socket.is_client = is_client;
                        socket.uid = uid;
                        socket.auth = updateList(uid, socket.id, 0, is_client);
                        socket.room = "comment-" + aid;
                        socket.type = "comment";
                        socket.join(socket.room);
                        socket.emit("on_success", {message: "User " + uid + " successfully joined to comments"});
                        logError(uid + ' joined the comment', false);
                    }else{
                        socket.emit('on_error', {message: "Invalid API Key"});
                        logError(uid + ' invalid api key', false);
                    }
                }
            });
        });

        socket.on('commentPost', function(data){
            if(!authenticated(socket) || socket.type != 'comment'){
                socket.emit('on_error', {message: "You are not authenticated to post comments"});
                logError('user not authenicated to post comment, uid: ' + socket.uid, false);
                return;
            }
            var message = validate(data.message);
            var aid = validate(data.aid);

            //incase of new comment it should be 0, else uid of parent comment
            var reply_to = validate(data.reply_to); 

            //incase of new comment -> client = 1, user = 0
            //incase of reply -> client key of parent comment
            var is_reply_on_user_is_client = validate(data.is_on_reply_client);

            var pci = validate(data.pci); //this can be 0 is main comment, in case of reply it will be ID to main comment

            if(!message || !aid){
                socket.emit('on_error', {message: "Parameter missing"});
                logError('comment post params missing', false);
            }else{
                database.postCommentOrReply(socket.uid, socket.is_client, message, aid, pci, function(err, ret){
                    if(err){
                        socket.emit('on_error', {message: 'Comment not saved, we have notified engineering team'});
                        logError('comment not saved: ' + err, true);
                    }
                    else{
                        var data = {
                            id : ret,
                            pci: pci,
                            uid: socket.uid,
                            message: message,
                            name: socket.name,
                            brand: socket.brand,
                            image: socket.image,
                            time: Math.round((new Date()).getTime() / 1000),
                            aid: aid,
                            client: socket.is_client
                        };
                        //send to all users including poster
                        io.in(socket.room).emit('new_comment', data);
                        logError('new comment posted in activity: ' + aid, false);
                    }
                });

                if(reply_to > 0){
                    //send notification to PCI user, if not admin
                    //do not send notification to user itself
                    var msg = {
                        'msg' : '0xDEADBEEF', //user for comments
                        'hash' : aid
                    };
                    if(!is_reply_on_user_is_client){
                        notif.sendNotification(database, reply_to, socket.uid, socket.name + " replied to your comment", msg);
                    }
                    else{
                        //someone replied on a comment posted by client
                        if(reply_to != socket.uid){
                            //replied is not client itself
                            var hash = md5('survey' + aid);
                            var payload = {
                                'title': 'Opinia Census',
                                'body' : socket.name + ' replied to your comment',
                                'url'  : 'https://opiniacensus.com/Analytics/index/' + hash
                            };
                            notif.sendClientNotification(database, reply_to, payload);
                        }
                    }

                    if(reply_to != socket.uid){
                        //insert notification
                        var uid = reply_to;
                        var uid_isclient = is_reply_on_user_is_client;
                        var from_id = socket.uid;
                        var from_isclient = socket.is_client;
                        var msg = socket.name + " replied to your comment";
                        var nid = aid;
                        var type = "0xDEADBEEF"; //user for comment
                        database.insertNotification(uid, uid_isclient, from_id, from_isclient, msg, nid, type, null);
                    }
                }
            }
        });

        socket.on('initChat', function(data){
            var uid = validate(data.uid);
            var key = validate(data.api_key);
            var is_client = parseInt(validate(data.is_client));

            database.authenticateUser(uid, key, is_client, function(err, ret){
                if(err){
                    logError(err, true);
                    socket.emit('on_error', {message: "An error occurred, we have notified engineering team"});
                }else{
                    if(ret){
                        if(is_client){
                            socket.name = ret.client_name;
                            socket.brand = ret.brand_name;
                            socket.image = ret.logo_image;
                        }else{
                            socket.name = ret.name;
                        }
                        socket.is_client = is_client;
                        socket.uid = uid;
                        socket.auth = updateList(uid, socket.id, 0, is_client);
                        socket.type = "group";
                        //subscribe user to all his groups
                        if(is_client) client_group_list[uid] = [];
                        else user_group_list[uid] = [];
                        database.findUserGroups(uid, is_client, function(err, ret){
                            if(err){
                                logError(err, true);
                                socket.emit('on_error', {message: "An error occurred while adding you to your groups. We have notified engineering team"});
                            }else{
                                var group_ids = ret;
                                for(var i=0; i < group_ids.length;i++){
                                    socket.join('group-' + group_ids[i].group_id);
                                    //notify everyone that user is online
                                    io.in('group-' + group_ids[i].group_id).emit('online', {uid: socket.uid, name: socket.name, client: socket.is_client});
                                    if(is_client) client_group_list[uid].push(group_ids[i].group_id);
                                    else user_group_list[uid].push(group_ids[i].group_id);
                                }
                            }
                        });
                        socket.emit("on_success", {message: "User " + uid + " successfully joined chats"});
                        logError(uid + ' joined the chat', false);
                    }else{
                        socket.emit('on_error', {message: "Invalid API Key"});
                        logError(uid + ' invalid api key', false);
                    }
                }
            });
        });

        socket.on('send_message', function(data){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }
            var msg = validate(data.body).toString();
            var type = validate(data.mtype);
            var mdata = validate(data.mtdata);
            var group_id = validate(data.group_id); if(group_id == 0) return;
            var uuid = validate(data.uuid); if(uuid == 0) return;
            var reply = 0;
            var reply_mid = 0;
            var room = "group-" + group_id;
            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to post in this room"});
                return;
            }

            if(type == 2){
                if(!isOurURL(mdata) || !isAllowedFile(mdata)){
                    socket.emit('on_error', {message: "Invalid file link provided!"});
                    logError("Invalid file link is submitted or file type is not allowed, id: " + socket.uid + " client: " + socket.is_client + " link: " + mdata);
                    return;
                }
            }
            if(type == 3){
                if(!isOurURL(mdata) || !isAllowedImage(mdata)){
                    socket.emit('on_error', {message: "Invalid file link provided!"});
                    logError("Invalid link is submitted or image type not allowed, id: " + socket.uid + " client: " + socket.is_client + " link: " + mdata);
                    return;
                }
            }
            if(type == 4){
                if(mdata.length != 11){
                    socket.emit('on_error', {message: "Invalid file link provided!"});
                    logError("Invalid link is submitted or invalid youtube id is submitted, id: " + socket.uid + " client: " + socket.is_client + " link: " + mdata);
                    return;
                }
            }

            //now insert into database and send recvd ack back to sender
            database.sendMessage(msg, type, mdata, group_id, socket.uid, socket.is_client, reply, reply_mid, uuid, function(err, ret){
                if(err){
                    logError(err, true);
                    socket.emit('on_error', {message: "An error occurred while saving message, we have notified engineering team"});
                }else{
                    var rep = ret;
                    if(rep.id){
                        //send message to all users at once, except sender
                        var data_to_send = {
                            id: rep.id,
                            from_uid: socket.uid,
                            is_client: socket.is_client,
                            group_id: group_id,
                            body: msg,
                            mtype: parseInt(type),
                            mtdata: mdata,
                            is_reply: reply,
                            reply_mid: reply_mid,
                            name: socket.name,
                            brand_name: socket.brand,
                            logo: socket.image,
                            time: Math.round((new Date()).getTime() / 1000),
                            status: 1
                        };
                        socket.to(room).emit('new_message', data_to_send);
                        socket.emit('received', {key: rep.key, id: rep.id}); //send back ack

                        database.setLastReceived(socket.uid, socket.is_client, group_id, rep.id, null);
                        database.setLastSeen(socket.uid, socket.is_client, group_id, rep.id, null);

                        //get all group users and send notification to offline users
                        database.findGroupUsers(group_id, function(e, r){
                            var users = r;
                            for(var i = 0; i < users.length; i++){
                                //do not send notification to sender itself or to client
                                if(socket.uid != users[i].user_id && users[i].is_client == 0){
                                    var notif_msg = {
                                        'msg' : '0xDEADDEEF', //for chat
                                        'hash' : ''
                                    };
                                    //check if user is not online
                                    var online = getOnlineUser(users[i].user_id, 0); //non client
                                    if(online){
                                        var socket_id = online.socketid;
                                        if(!io.sockets.connected[socket_id]){
                                            //user is not online
                                            notif.sendNotification(database, users[i].user_id, socket.uid, socket.name + " sent a message", notif_msg);
                                        }
                                    }else{
                                        notif.sendNotification(database, users[i].user_id, socket.uid, socket.name + " sent a message", notif_msg);
                                    }
                                }else{
                                    if(users[i].is_client && socket.uid != users[i].user_id){
                                        var online = getOnlineUser(users[i].user_id, 1); //client
                                        if(online){
                                            var socket_id = online.socketid;
                                            if(!io.sockets.connected[socket_id]){
                                                var payload = {
                                                    'title': 'Opinia Census',
                                                    'body' : socket.name + ' sent a message',
                                                    'url'  : 'https://opiniacensus.com/Dashboard#/chat'
                                                };
                                                notif.sendClientNotification(database, reply_to, payload);
                                            }
                                        }else{
                                            var payload = {
                                                'title': 'Opinia Census',
                                                'body' : socket.name + ' replied to your comment',
                                                'url'  : 'https://opiniacensus.com/Dashboard#/chat'
                                            };
                                            notif.sendClientNotification(database, reply_to, payload);
                                        }
                                    }
                                }
                            }
                        });
                    }else{
                        socket.emit('not_saved', {key: rep.key});
                    }
                }
            });
        });

        socket.on('msg_received', function(data){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }
            var uid = socket.uid;
            var is_client = socket.is_client;
            var msg_id = validate(data.msg_id); if(msg_id == 0) return;
            var group_id = validate(data.group_id); if(group_id == 0) return;
            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to participate in this room"});
                return;
            }
            database.setLastReceived(uid, is_client, group_id, msg_id, null);
            var room = "group-" + group_id;
            socket.to(room).emit('msg_delivered', {id: msg_id, group: group_id});
        });

        socket.on('msg_seen', function(data){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }
            var uid = socket.uid;
            var is_client = socket.is_client;
            var msg_id = validate(data.msg_id); if(msg_id == 0) return;
            var group_id = validate(data.group_id); if(group_id == 0) return;
            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to participate in this room"});
                return;
            }
            database.setLastSeen(uid, is_client, group_id, msg_id, null);
            var room = "group-" + group_id;
            socket.to(room).emit('msg_read', {id: msg_id, group: group_id});
        });

        socket.on('start_typing', function(data){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }

            var group_id = validate(data.group_id); if(group_id == 0) return;
            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to post in this room"});
                return;
            }

            socket.to('group-' + group_id).emit('start_typing', {message: socket.name + " is typing...", uid: socket.uid, client: socket.is_client, group: group_id});
        });

        socket.on('end_typing', function(data){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }
            var group_id = validate(data.group_id); if(group_id == 0) return;
            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to post in this room"});
                return;
            }

            socket.to('group-' + group_id).emit('end_typing', {message: socket.name + " stopped typing", uid: socket.uid, client: socket.is_client, group: group_id});
        })

        socket.on('remove_user', function(data, fn){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }
            var uid = socket.uid;
            var is_client = socket.is_client;
            var group_id = validate(data.group_id);
            var group_users = validate(data.group_users).toString();
            group_users = group_users.split(",");

            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to post in this room"});
                return;
            }

            if(is_client){
                //user is a client, remove all users which is provided
                group_users.forEach(element => {
                    database.removeParticipant(element, 0, group_id, true, null);
                    var user = getOnlineUser(element, 0);
                    if(user){
                        if(user.socketid){
                            //notify user that room is dead for him/her cuz user is removed
                            io.to(user.socketid).emit('room_dead', {id: group_id});
                            var user_socket = io.sockets.connected[user.socketid];
                            user_socket.leave('group-' + group_id);
                        }
                    }
                    database.getPushtokenUser(element, function(e, r){
                        if(e){
                            logError(e, true);
                        }else{
                            if(r){
                                //notify room that a user left
                                var data_to_send = {
                                    id: 0,
                                    from_uid: r.id,
                                    is_client: 0,
                                    group_id: group_id,
                                    body: r.name + ' - removed from this conversation',
                                    mtype: 6,
                                    mtdata: '',
                                    is_reply: 0,
                                    reply_mid: 0,
                                    name: r.name,
                                    brand_name: '',
                                    logo: '',
                                    time: Math.round((new Date()).getTime() / 1000)
                                };
                                //notify everyone
                                io.in('group-' + group_id).emit('new_message', data_to_send);
                            }
                        }
                    }); 
                });
            }
            if(fn) fn(true);
            socket.emit("on_success", {message: "User(s) removed from conversation"});
        });

        socket.on('deletegroup', function(data, fn){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }

            var uid = socket.uid;
            var is_client = socket.is_client;
            var group_id = validate(data.group_id);

            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to post in this room"});
                return;
            }

            //if client disable this group, else just remove that user only
            if(is_client){
                database.disableGroup(group_id, null);
                //notify room is closed
                var data_to_send = {
                    id: 0,
                    from_uid: socket.uid,
                    is_client: 1,
                    group_id: group_id,
                    body: socket.name + ' - closed this conversation',
                    mtype: 6,
                    mtdata: '',
                    is_reply: 0,
                    reply_mid: 0,
                    name: socket.name,
                    brand_name: socket.brand,
                    logo: socket.image,
                    time: Math.round((new Date()).getTime() / 1000)
                };
                io.in('group-' + group_id).emit('new_message', data_to_send);
                io.in('group-' + group_id).emit('room_dead', {id: group_id});

            }else{
                database.removeParticipant(uid, 0, group_id, false, null);
                //notify room that a user left
                var data_to_send = {
                    id: 0,
                    from_uid: socket.uid,
                    is_client: 0,
                    group_id: group_id,
                    body: socket.name + ' - left this conversation',
                    mtype: 6,
                    mtdata: '',
                    is_reply: 0,
                    reply_mid: 0,
                    name: socket.name,
                    brand_name: '',
                    logo: '',
                    time: Math.round((new Date()).getTime() / 1000)
                };
                io.in('group-' + group_id).emit('new_message', data_to_send);
                socket.emit('room_dead', {id: group_id});
                socket.leave('group-' + group_id);
            }
            if(fn) fn(true);
            socket.emit("on_success", {message: "User(s) removed from conversation"});
        });

        socket.on('add_user', function(data, fn){
            if(!authenticated(socket) || socket.type != 'group'){
                socket.emit('on_error', {message: "You are not authenticated to post messages"});
                logError('user not authenicated to post messages, uid: ' + socket.uid, false);
                return;
            }

            var uid = socket.uid;
            var is_client = 1; //socket.is_client;
            var group_id = validate(data.group_id);
            var group_users = validate(data.group_users).toString();
            group_users = group_users.split(",");

            if(!authorizedRoom(socket, group_id)){
                socket.emit('on_error', {message: "You are not authorized to post in this room"});
                return;
            }

            database.getSingleRoom(uid, is_client, group_id, function(err, ret){
                if(err){
                    logError(err, res, true);
                }else{
                    //members can be added only in group chats
                    if(ret.length){
                        if(ret[0].type == 2){
                            //add users
                            group_users.forEach(element => {
                                database.alreadyInGroup(element, 0, group_id, function(e, r){
                                    if(e){
                                        logError(res, e, true);
                                    }else{
                                        if(!r){
                                            database.addParticipant(group_id, element, 0, null);
                                            //send notficication that user is added
                                            var notif_msg = {
                                                'msg' : '0xDEADDEEF', //for chat
                                                'hash' : ''
                                            };
                                            notif.sendNotification(database, element, "0xDEADBEEF", "You are added in a chat", notif_msg);
                                            database.getPushtokenUser(element, function(er, rr){
                                                if(er){
                                                    logError(er, true);
                                                }else{
                                                    if(rr){
                                                        //notify room that a user left
                                                        var data_to_send = {
                                                            id: 0,
                                                            from_uid: rr.id,
                                                            is_client: 0,
                                                            group_id: group_id,
                                                            body: rr.name + '- added in this conversation',
                                                            mtype: 5,
                                                            mtdata: '',
                                                            is_reply: 0,
                                                            reply_mid: 0,
                                                            name: rr.name,
                                                            brand_name: '',
                                                            logo: '',
                                                            time: Math.round((new Date()).getTime() / 1000)
                                                        };
                                                        //notify everyone
                                                        io.in('group-' + group_id).emit('new_message', data_to_send);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            });
                            socket.emit("on_success", {message: "Members added in group"});
                            if(fn) fn(true);
                        }else{
                            socket.emit('on_error', {message: "Members can only be added in group chat"});
                            if(fn) fn(false);
                        }
                    }
                    else{
                        socket.emit('on_error', {message: "Requested group not found"});
                        if(fn) fn(false);
                    }
                }
            });
        });

        socket.on('disconnect', function(){
            if(socket.uid){
                var uid = socket.uid;
                disconnectUser(io, socket);
                logError('user ' + uid + ' disconnected', false);
            }
            else logError('anon user disconnected', false);
        });
    });
}